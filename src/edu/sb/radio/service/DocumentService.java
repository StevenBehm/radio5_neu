package edu.sb.radio.service;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.sb.radio.persistence.Document;
import edu.sb.radio.util.HashCodes;
import edu.sb.radio.util.RestJpaLifecycleProvider;

@Path("documents")
public class DocumentService {
	
	static private final String QUERY = "select d from Document as d where hash = :hash";
	
	
	@GET
	@Path("{id}")
	@Produces(MediaType.WILDCARD)
	public Response findDocument (
			@PathParam("id") @PositiveOrZero final long documentIdentity
	) {
		final String getNegotiationQuery = "select d from Negotiation as d where document.identity = :identity";
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Document document = entityManager
				.createQuery(getNegotiationQuery, Document.class)
				.setParameter("identity", documentIdentity)
				.getResultList()
				.stream()
				.findFirst()
				.orElse(null);
		
		if(document == null) throw new ClientErrorException(NOT_FOUND);
		return Response.ok(document.getContent(), document.getType()).build();
	}
	
	
	@POST
	@Consumes(MediaType.WILDCARD)
	@Produces(MediaType.TEXT_PLAIN)
	public long modifyDocument (
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) @Positive final long requesterIdentity,
			@HeaderParam("Content-Type") final String contentType,
			@NotNull final byte[] content
	) {

		final String contentHash = HashCodes.sha2HashText(256, content);

		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		Document document = entityManager
			.createQuery(QUERY, Document.class)
			.setParameter("hash", contentHash)
			.getResultList()
			.stream()
			.findFirst()
			.orElse(null);
		
		if(document == null) document = new Document(content);
		document.setType(contentType);	
		
		if(document.getIdentity() == 0) 
			entityManager.persist(document);
		else
			entityManager.flush();
		
		try {
			entityManager.getTransaction().commit();
		} catch (RollbackException e) {
			throw new ClientErrorException(Status.CONFLICT);
		} finally {
			entityManager.getTransaction().begin();
		}
		
		return document.getIdentity();
	}
}
