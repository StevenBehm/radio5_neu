package edu.sb.radio.service;

import static javax.ws.rs.Priorities.AUTHENTICATION;
import static javax.ws.rs.core.HttpHeaders.WWW_AUTHENTICATE;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

import java.util.List;

import javax.annotation.Priority;
import javax.persistence.EntityManager;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import edu.sb.radio.persistence.Person;
import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.HashCodes;
import edu.sb.radio.util.HttpCredentials;
import edu.sb.radio.util.RestCredentials;
import edu.sb.radio.util.RestJpaLifecycleProvider;


/**
 * JAX-RS filter provider that performs HTTP "basic" authentication on any REST service request. This aspect-oriented
 * design swaps "Authorization" headers for "Requester-Identity" during authentication.
 */
@Provider
@Priority(AUTHENTICATION)
@Copyright(year = 2017, holders = "Sascha Baumeister")

public class BasicAuthenticationFilter implements ContainerRequestFilter {
	/**
	 * HTTP request header for the authenticated requester's identity.
	 */
	static public final String REQUESTER_IDENTITY = "X-Requester-Identity";
	static private final String QUERY_PERSON = "select p from Person as p where p.email = :email";

	/**
	 * Performs HTTP "basic" authentication by calculating a password hash from the password contained in the request's
	 * "Authorization" header, and comparing it to the one stored in the person matching said header's username. The
	 * "Authorization" header is consumed in any case, and upon success replaced by a new "Requester-Identity" header that
	 * contains the authenticated person's identity. The filter chain is aborted in case of a problem.
	 * @param requestContext {@inheritDoc}
	 * @throws NullPointerException if the given argument is {@code null}
	 * @throws ClientErrorException (400) if the "Authorization" header is malformed, or if there is a pre-existing
	 *         "Requester-Identity" header
	 */
	public void filter (final ContainerRequestContext requestContext) throws NullPointerException, ClientErrorException {		
		if(requestContext.getHeaders().containsKey(REQUESTER_IDENTITY)) throw new ClientErrorException(Status.BAD_REQUEST);
		final List<String> authorizationHeaders = requestContext.getHeaders().remove(HttpHeaders.AUTHORIZATION);
		final String textCredentials = authorizationHeaders == null || authorizationHeaders.isEmpty() ? null : authorizationHeaders.get(0);

		if(textCredentials != null) {
			final HttpCredentials.Basic restCredentials = RestCredentials.newBasicInstance(textCredentials);
			final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
            
			final Person requester = entityManager
				.createQuery(QUERY_PERSON, Person.class)
				.setParameter("email", restCredentials.getName())
				.getResultList() //es wird nur einmal eine richtige Datenbank abfrage ausgeführt pro benutzer, wegen @CacheIndex by email
				.stream()
				.findFirst()
				.orElse(null);
			
			if(requester != null) {
				final String leftHash = requester.getPasswordHash();
				final String rightHash = HashCodes.sha2HashText(256, restCredentials.getPassword());
				
				if(leftHash.equals(rightHash)) {
					requestContext.getHeaders().putSingle(REQUESTER_IDENTITY, Long.toString(requester.getIdentity()));
					
					return;
				}
			}
		}
		requestContext.abortWith(Response.status(UNAUTHORIZED).header(WWW_AUTHENTICATE, "Basic").build());
	}
}