package edu.sb.radio.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import edu.sb.radio.persistence.Document;
import edu.sb.radio.persistence.Negotiation;
import edu.sb.radio.persistence.Person;
import edu.sb.radio.persistence.Person.Group;
import edu.sb.radio.persistence.Track;
import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.RestJpaLifecycleProvider;

@Path("people")
@Copyright(year=2020, holders="Radio5 Group")
public class PersonService {
	static private final String HEADER_SET_PASSWORD = "X-Set-Password";
	
	/*static private final String QUERY = "select distinct p.identity from Person as p left outer join Negotiation as n on n.negotiator.identity = p.identity where "
			+ "(:lowerModificationTimestamp is null or p.modificationTimestamp >= :lowerModificationTimestamp) and " 
			+ "(:upperModificationTimestamp is null or p.ModificationTimestamp <= :upperModificationTimestamp) and " 
			+ "(:email is null or p.email = :email) and "
			+ "(:group is null or p.group = :group) and "
			+ "(:country is null or p.address.country in :country) and"
			+ "(:city is null or p.address.city in :city) and"
			+ "(:street is null or p.address.street in :street) and"
			+ "(:postcodes is null or p.address.postcode in :postcode) and"
			+ "(:title is null or p.name.title in :title) and"
			+ "(:forename is null or p.name.given in :forename) and"
			+ "(:surname is null or p.name.family in :surname) and"
			+ "(:type is null or n.type = :type) and"
			+ "(:offer is null or n.offer in :offer) and"
			+ "(:answer is null or n.answer in :answer) and"
			+ "(:offering is null or (n.offer is not null and (n.answer is null = :offering))) and"
			+ "(:answering is null or (n.offer is not null and (n.answer is not null = :answering)))";*/
	
	static private final String QUERY = "select p.identity from Person as p where "
            + "(:lowerModificationTimestamp is null or p.modificationTimestamp >= :lowerModificationTimestamp) and "
            + "(:upperModificationTimestamp is null or p.modificationTimestamp <= :upperModificationTimestamp) and "
            + "(:email is null or p.email = :email) and "
            + "(:group is null or p.group = :group) and "
            + "(:forename is null or p.name.given = :forename) and "
            + "(:surname is null or p.name.family = :surname) and "
            + "(:title is null or p.name.title = :title) and "
            + "(:street is null or p.address.street = :street) and "
            + "(:country is null or p.address.country = :country) and "
            + "(:city is null or p.address.city = :city) and "
            + "(:postcode is null or p.address.street = :postcode)";
           // + "(:type is null or n.type = :type)";
           // + "(:offer is null or n.offer = :offer) and "
           // + "(:answer is null or n.answer = :answer) and "
           // + "(:offering is null or (n.offer is not null and (n.answer is null = :offering))) and "
           // + "(:answering is null or (n.offer is not null and (n.answer is not null = :answering)))";

	@GET
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Person[] queryPeople (
			@QueryParam("resultOffset") @Positive final Integer resultOffset,
			@QueryParam("resultLimit") @Positive final Integer resultLimit,
			@QueryParam("lowerModificationTimestamp") @Positive final Long lowerModificationTimestamp,
			@QueryParam("upperModificationTimestamp") @Positive final Long upperModificationTimestamp,
			@QueryParam("email")final String email,
			@QueryParam("group") final Group group,
			@QueryParam("country") final String country,
			@QueryParam("city") final String city,
			@QueryParam("street") final String street,		
			@QueryParam("postcode") final String postcode,
			@QueryParam("title") final String title,
			@QueryParam("forename") final String forename,
			@QueryParam("surname") final String surname
			//@QueryParam("type") final Negotiation.Type type,
			//@QueryParam("offer") final String offer,
			//@QueryParam("answer") final String answer,
			//@QueryParam("offering") final Boolean offering, 
			//@QueryParam("answering") final Boolean answering
	) {	
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final TypedQuery<Long> query = entityManager.createQuery(QUERY, Long.class);
		if (resultOffset != null) query.setFirstResult(resultOffset);
		if (resultLimit != null) query.setMaxResults(resultLimit);
		
		final Person[] people = query
				.setParameter("lowerModificationTimestamp", lowerModificationTimestamp)
				.setParameter("upperModificationTimestamp", upperModificationTimestamp)
				.setParameter("email", email)		
				.setParameter("group", group)			
				.setParameter("country", country)
				.setParameter("city", city)			
				.setParameter("street", street)				
				.setParameter("postcode", postcode)				
				.setParameter("title", title)				
				.setParameter("forename", forename)			
				.setParameter("surname", surname)	
				//.setParameter("type", type)
				//.setParameter("offer", offer)				
				//.setParameter("answer", answer)				
				//.setParameter("offering", offering)
				//.setParameter("answering", answering)
				.getResultList()
				.stream()
				.map(identity -> entityManager.find(Person.class, identity))
				.filter(person -> person != null)
				.sorted()
				.toArray(Person[]::new);
		return people;
	}
	
	
	@GET
	@Path("{id}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Person findPerson (
			@PathParam("id") @PositiveOrZero final long personIdentity,
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) @Positive final long requesterIdentity
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final long identity = personIdentity == 0 ? requesterIdentity : personIdentity; //AUSNAHME!!!!
		final Person person = entityManager.find(Person.class, identity);
		if (person == null) throw new ClientErrorException(NOT_FOUND);
		return person;
	}
	
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes({ APPLICATION_JSON, APPLICATION_XML })
	public long postPerson (
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) @Positive final long requesterIdentity,
			@HeaderParam(HEADER_SET_PASSWORD) @Size(min=4) final String password,
			@QueryParam("avatarReference") @Positive Long avatarReference,
			@NotNull @Valid final Person personTemplate 
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final Person requester = entityManager.find(Person.class, requesterIdentity);
		if (requester == null) throw new ClientErrorException(NOT_FOUND);
		if (requester.getIdentity() != personTemplate.getIdentity() && requester.getGroup() != Person.Group.ADMIN) throw new ClientErrorException(Status.BAD_REQUEST);

		final boolean persistMode = personTemplate.getIdentity() == 0;
		final Person person;
		
		if(persistMode) {
			person = new Person();
			if(avatarReference == null) avatarReference = 1L;
		} else {
			person = entityManager.find(Person.class, personTemplate.getIdentity());
			if(person == null) throw new ClientErrorException(Status.NOT_FOUND);
		}
		
		person.setVersion(personTemplate.getVersion());
		person.setModificationTimestamp(personTemplate.getModificationTimestamp());
		person.setEmail(personTemplate.getEmail());
		person.getName().setTitle(personTemplate.getName().getTitle());
		person.getName().setFamily(personTemplate.getName().getFamily());
		person.getName().setGiven(personTemplate.getName().getGiven());
		person.getAddress().setStreet(personTemplate.getAddress().getStreet());
		person.getAddress().setPostcode(personTemplate.getAddress().getPostcode());
		person.getAddress().setCity(personTemplate.getAddress().getCity());
		person.getAddress().setCountry(personTemplate.getAddress().getCountry());
		person.getPhones().retainAll(personTemplate.getPhones());
		person.getPhones().addAll(personTemplate.getPhones());
		
		if(requester.getGroup() == Person.Group.ADMIN) person.setGroup(personTemplate.getGroup());
		if(password != null) person.setPasswordHash(password);
		if(avatarReference != null) {
			Document avatar = entityManager.find(Document.class, avatarReference);
			if(avatar == null) throw new ClientErrorException(Status.NOT_FOUND);
			person.setAvatar(avatar);
		}
		
		if(persistMode) 
			entityManager.persist(person);
		else
			entityManager.flush();
		
		try {
			entityManager.getTransaction().commit();
		} catch (RollbackException e) {
			throw new ClientErrorException(Status.CONFLICT);
		} finally {
			entityManager.getTransaction().begin();
		}
		
		return person.getIdentity();
	}
	
	
	@GET
	@Path("{id}/tracks")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Track[] findTracks (
			@PathParam("id") @PositiveOrZero final long personIdentity
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Person person = entityManager.find(Person.class, personIdentity);
		if (person == null) throw new ClientErrorException(NOT_FOUND);
		
		return person
				.getTracks()
				.stream()
				.sorted()
				.toArray(Track[]::new);
	}
	
	@GET
	@Path("{id}/negotiations")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Negotiation[] findNegotiations (
			@PathParam("id") @PositiveOrZero final long personIdentity	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Person person = entityManager.find(Person.class, personIdentity);
		if (person == null) throw new ClientErrorException(NOT_FOUND);
		
		return person
				.getNegotiations()
				.stream()
				.sorted()
				.toArray(Negotiation[]::new);
	}
	
}