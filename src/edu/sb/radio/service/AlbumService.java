package edu.sb.radio.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import edu.sb.radio.persistence.Album;
import edu.sb.radio.persistence.Document;
import edu.sb.radio.persistence.Person;
import edu.sb.radio.util.RestJpaLifecycleProvider;

@Path("albums")
public class AlbumService {
	
	static private final String QUERY = "select a.identity from Album as a where"
			+ "(:title is null or a.title = :title) and"			
			+ "(:lowerReleaseYear is null or a.releaseYear >= :lowerReleaseYear) and"
			+ "(:upperReleaseYear is null or a.releaseYear <= :upperReleaseYear) and"
			+ "(:lowerTrackCount is null or a.trackCount >= :lowerTrackCount) and"
			+ "(:upperTrackCount is null or a.trackCount <= :upperTrackCount)";
		
	@GET
	@Path("{id}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Album findAlbum (
			@PathParam("id") @PositiveOrZero final long albumIdentity
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Album album = entityManager.find(Album.class, albumIdentity);
		if (album == null) throw new ClientErrorException(NOT_FOUND);
		return album;
	}
	
	
	@GET
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Album[] queryAlbums (
			@QueryParam("resultOffset") @Positive final Integer resultOffset,
			@QueryParam("resultLimit") @Positive final Integer resultLimit,
			@QueryParam("title") final String title,
			@QueryParam("lowerReleaseYear") final Short lowerReleaseYear,
			@QueryParam("upperReleaseYear") final Short upperReleaseYear,
			@QueryParam("lowerTrackCount") @Positive final Byte lowerTrackCount,
			@QueryParam("upperTrackCount") @Positive final Byte upperTrackCount
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final TypedQuery<Long> query = entityManager.createQuery(QUERY, Long.class);
		if (resultOffset != null) query.setFirstResult(resultOffset);
		if (resultLimit != null) query.setMaxResults(resultLimit);
		
		final Album[] albums = query
				.setParameter("title", title)
				.setParameter("lowerReleaseYear", lowerReleaseYear)
				.setParameter("upperReleaseYear", upperReleaseYear)
				.setParameter("lowerTrackCount", lowerTrackCount)
				.setParameter("upperTrackCount", upperTrackCount)
				.getResultList()
				.stream()
				.map(identity -> entityManager.find(Album.class, identity))
				.filter(album -> album != null)
				.sorted()
				.toArray(Album[]::new);
		return albums;
	}
	
	
	@POST
	@Consumes({ APPLICATION_JSON, APPLICATION_XML })
	@Produces(MediaType.TEXT_PLAIN)
	public long modifyAlbum (
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) final long requesterIdentity,
			@QueryParam("coverReference") @Positive final Long coverReference,
			@NotNull @Valid final Album albumTemplate
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final Person requester = entityManager.find(Person.class, requesterIdentity);
		if(requester == null) throw new ClientErrorException(Status.FORBIDDEN);
		if(requester.getGroup() != Person.Group.ADMIN) throw new ClientErrorException(Status.BAD_REQUEST);
		
		final boolean persistMode = albumTemplate.getIdentity() == 0;
		final Album album;
		
		if(persistMode) {
			album = new Album();
		} else {
			album = entityManager.find(Album.class, albumTemplate.getIdentity());	
			if (album == null) throw new ClientErrorException(Status.NOT_FOUND);
		}
		
		album.setVersion(albumTemplate.getVersion());
		album.setModificationTimestamp(albumTemplate.getModificationTimestamp());
		album.setTitle(albumTemplate.getTitle());
		album.setReleaseYear(albumTemplate.getReleaseYear());
		album.setTrackCount(albumTemplate.getTrackCount());
		
		if(coverReference != null) {
			final Document cover = entityManager.find(Document.class, coverReference);	
			if (cover == null) throw new ClientErrorException(Status.NOT_FOUND);
			album.setCover(cover);
		}
		
		if(persistMode) 
			entityManager.persist(album);
		else
			entityManager.flush();
		
		try {
			entityManager.getTransaction().commit();
		} catch (RollbackException e) {
			throw new ClientErrorException(Status.CONFLICT);
		} finally {
			entityManager.getTransaction().begin();
		}
		
		return album.getIdentity();
	}
}
