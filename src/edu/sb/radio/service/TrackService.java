package edu.sb.radio.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.Collections;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import edu.sb.radio.persistence.Album;
import edu.sb.radio.persistence.Document;
import edu.sb.radio.persistence.Person;
import edu.sb.radio.persistence.Track;
import edu.sb.radio.util.RestJpaLifecycleProvider;

@Path("tracks")
public class TrackService {

	static private final String QUERY = "select t.identity from Track as t where "
		+ "(:lowerOrdinal is null or t.ordinal >= :lowerOrdinal) and " 
		+ "(:upperOrdinal is null or t.ordinal <= :upperOrdinal) and " 
		+ "(:ignoreNames = true or t.name in :names) and "
		+ "(:ignoreArtists = true or t.artist in :artists) and "
		+ "(:ignoreGenres = true or t.genre in :genres)";
	static private final String ARTISTS_QUERY = "select distinct t.artist from Track as t";
	static private final String GENRES_QUERY = "select distinct t.genre from Track as t";
	
	static private final Set<String> EMPTY_WORD_SINGLETON = Collections.singleton("");
	
	@GET
	@Path("{id}")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Track findTrack (
			@PathParam("id") @PositiveOrZero final long trackIdentity
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Track track = entityManager.find(Track.class, trackIdentity);
		if (track == null) throw new ClientErrorException(NOT_FOUND);
		return track;
	}
	
	
	@GET
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Track[] queryTracks (
			@QueryParam("resultOffset") @Positive final Integer resultOffset,
			@QueryParam("resultLimit") @Positive final Integer resultLimit,
			@QueryParam("name") @NotNull final Set<String> names,
			@QueryParam("genre") @NotNull final Set<String> genres,
			@QueryParam("artist") @NotNull final Set<String> artists,
			@QueryParam("lowerOrdinal") @PositiveOrZero final Byte lowerOrdinal,
			@QueryParam("upperOrdinal") @PositiveOrZero final Byte upperOrdinal
	) {	
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final TypedQuery<Long> query = entityManager.createQuery(QUERY, Long.class);
		if (resultOffset != null) query.setFirstResult(resultOffset);
		if (resultLimit != null) query.setMaxResults(resultLimit);
		
		final Track[] tracks = query
				.setParameter("lowerOrdinal", lowerOrdinal)
				.setParameter("upperOrdinal", upperOrdinal)
				.setParameter("ignoreNames", !names.isEmpty())
				.setParameter("names", names.isEmpty() ? EMPTY_WORD_SINGLETON : names)
				.setParameter("ignoreArtists", !artists.isEmpty())
				.setParameter("artists", artists.isEmpty() ? EMPTY_WORD_SINGLETON : artists)
				.setParameter("ignoreGenres", !genres.isEmpty())
				.setParameter("genres", genres.isEmpty() ? EMPTY_WORD_SINGLETON : genres)
				.getResultList()
				.stream()
				.map(id -> entityManager.find(Track.class, id))
				.filter(t -> t != null)
				.sorted()
				.toArray(Track[]::new);
		
		return tracks;
	}
	
	
	@POST
	@Consumes({ APPLICATION_JSON, APPLICATION_XML })
	@Produces(MediaType.TEXT_PLAIN)
	public long modifyTrack (
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) final long requesterIdentity,
			@QueryParam("albumReference") @Positive final Long albumReference,
			@QueryParam("recordingReference") @Positive final Long recordingReference,
			@NotNull @Valid final Track trackTemplate
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final Person requester = entityManager.find(Person.class, requesterIdentity);
		if(requester == null) throw new ClientErrorException(Status.FORBIDDEN);
		if(requester.getGroup() != Person.Group.ADMIN) throw new ClientErrorException(Status.BAD_REQUEST);
		
		final boolean persistMode = trackTemplate.getIdentity() == 0;
		
		final Track track;
		if (persistMode) {
			if (albumReference == null | recordingReference == null) throw new ClientErrorException(Status.BAD_REQUEST);
							
			track = new Track();
			track.setOwner(requester);
		} else {
			track = entityManager.find(Track.class, trackTemplate.getIdentity());	
			if (track == null) throw new ClientErrorException(Status.NOT_FOUND);
		}
		
		track.setVersion(trackTemplate.getVersion());
		track.setModificationTimestamp(trackTemplate.getModificationTimestamp());
		track.setArtist(trackTemplate.getArtist());
		track.setGenre(trackTemplate.getGenre());
		track.setName(trackTemplate.getName());
		track.setOrdinal(trackTemplate.getOrdinal());
		if (albumReference != null) {
			final Album album = entityManager.find(Album.class, albumReference);	
			if (album == null) throw new ClientErrorException(Status.NOT_FOUND);
			track.setAlbum(album);
		}
		if (recordingReference != null) {
			final Document recording = entityManager.find(Document.class, recordingReference);	
			if (recording == null) throw new ClientErrorException(Status.NOT_FOUND);
			track.setRecording(recording);
		}
		
		if(persistMode) 
			entityManager.persist(track);
		else
			entityManager.flush();
		
		try {
			entityManager.getTransaction().commit();
		} catch (RollbackException e) {
			throw new ClientErrorException(Status.CONFLICT);
		} finally {
			entityManager.getTransaction().begin();
		}
		
		if(persistMode && albumReference != null) entityManager.getEntityManagerFactory().getCache().evict(Album.class, albumReference);
		if(persistMode) entityManager.getEntityManagerFactory().getCache().evict(Person.class, requester.getIdentity());
		return track.getIdentity();
	}
	

	@GET
	@Path("artists")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public String[] queryArtists () {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final String[] artists = entityManager
				.createQuery(ARTISTS_QUERY, Track.class)
				.getResultList()
				.stream()
				.toArray(String[]::new);
		
		return artists;
	}
	
	
	@GET
	@Path("genres")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public String[] queryGenres () {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		
		final String[] genres = entityManager
				.createQuery(GENRES_QUERY, Track.class)
				.getResultList()
				.stream()
				.toArray(String[]::new);
		
		return genres;
	}
}