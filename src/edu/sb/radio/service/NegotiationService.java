package edu.sb.radio.service;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;

import edu.sb.radio.persistence.Negotiation;
import edu.sb.radio.persistence.Person;
import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.RestJpaLifecycleProvider;

@Path("negotiations")
@Copyright(year=2020, holders="Radio5 Group")
public class NegotiationService {
	
	static private final String QUERY = "select n.negotiator.identity from Negotiation as n where "
            + "(:lowerModificationTimestamp is null or n.modificationTimestamp >= :lowerModificationTimestamp) and "
            + "(:upperModificationTimestamp is null or n.modificationTimestamp <= :upperModificationTimestamp) and "
            + "(:type is null or n.type = :type) and "
            + "(:offer is null or n.offer = :offer) and "
            + "(:answer is null or n.answer = :answer) and "
            + "(:offering is null or (n.offer is not null and (n.answer is null = :offering))) and "
            + "(:answering is null or (n.offer is not null and (n.answer is not null = :answering))) ";
	
	
	@GET
	@Path("negotiators")
	@Produces({ APPLICATION_JSON, APPLICATION_XML })
	public Person[] queryNegotiators (
			@QueryParam("resultOffset") @Positive final Integer resultOffset,
			@QueryParam("resultLimit") @Positive final Integer resultLimit,
			@QueryParam("lowerModificationTimestamp") @Positive final Long lowerModificationTimestamp,
			@QueryParam("upperModificationTimestamp") @Positive final Long upperModificationTimestamp,
			@QueryParam("type") final Negotiation.Type type,
			@QueryParam("offer") final String offer,
			@QueryParam("answer") final String answer,
			@QueryParam("offering") final Boolean offering, 
			@QueryParam("answering") final Boolean answering
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final TypedQuery<Long> query = entityManager.createQuery(QUERY, Long.class);
		if (resultOffset != null) query.setFirstResult(resultOffset);
		if (resultLimit != null) query.setMaxResults(resultLimit);
		
		final Person[] people = query
				.setParameter("lowerModificationTimestamp", lowerModificationTimestamp)
				.setParameter("upperModificationTimestamp", upperModificationTimestamp)
				.setParameter("type", type)
				.setParameter("offer", offer)				
				.setParameter("answer", answer)				
				.setParameter("offering", offering)
				.setParameter("answering", answering)
				.getResultList()
				.stream()
				.map(identity -> entityManager.find(Person.class, identity))
				.filter(person -> person != null)
				.sorted()
				.toArray(Person[]::new);
		return people;
	}
	
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes({ APPLICATION_JSON, APPLICATION_XML })
	public long modifyNegotiation (
			@HeaderParam(BasicAuthenticationFilter.REQUESTER_IDENTITY) @Positive final long requesterIdentity,
			@NotNull @Valid final Negotiation negotiationTemplate	
	) {
		final EntityManager entityManager = RestJpaLifecycleProvider.entityManager("radio");
		final Person requester = entityManager.find(Person.class, requesterIdentity);
		if (requester == null) throw new ClientErrorException(NOT_FOUND);
		
		final boolean persistMode = negotiationTemplate.getIdentity() == 0;
		final Negotiation negotiation;
		if(persistMode) {
			negotiation = new Negotiation();
			negotiation.setNegotiator(requester);
		} else {
			negotiation = entityManager.find(Negotiation.class, negotiationTemplate.getIdentity());
			if(negotiation == null) throw new ClientErrorException(Status.NOT_FOUND);
		}
		
		negotiation.setVersion(negotiationTemplate.getVersion());
		negotiation.setModificationTimestamp(negotiationTemplate.getModificationTimestamp());
		negotiation.setType(negotiationTemplate.getType());
		negotiation.setOffer(negotiationTemplate.getOffer());
		negotiation.setAnswer(negotiationTemplate.getAnswer());
		
		if(persistMode) 
			entityManager.persist(negotiation);
		else
			entityManager.flush();
		
		try {
			entityManager.getTransaction().commit();
		} catch (RollbackException e) {
			throw new ClientErrorException(Status.CONFLICT);
		} finally {
			entityManager.getTransaction().begin();
		}
		
		if(persistMode) entityManager.getEntityManagerFactory().getCache().evict(Person.class, requester.getIdentity());
		return negotiation.getIdentity();
	}
}
