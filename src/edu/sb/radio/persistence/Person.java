package edu.sb.radio.persistence;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.eclipse.persistence.annotations.CacheIndex;

import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.HashCodes;
import edu.sb.radio.util.JsonProtectedPropertyStrategy;

@Entity
@Table(schema = "radio", name = "Person", indexes = @Index(columnList = "email", unique = true))
@PrimaryKeyJoinColumn(name = "personIdentity")
@JsonbVisibility(JsonProtectedPropertyStrategy.class)
@XmlType @XmlRootElement
@Copyright(year=2020, holders="Radio5 Group")
public class Person extends BaseEntity {
	/*******************************************************************/
	//Membervariablen
	static public enum Group { ADMIN, USER }
	
	static private final String DEFAULT_PASSWORD = "change_it";
	static private final String DEFAULT_PASSWORD_HASH = HashCodes.sha2HashText(256, DEFAULT_PASSWORD);
	
	@NotNull @Email @Size(min = 3, max = 128)
	@Column(nullable = false, updatable = true, length = 128, unique = true)
	@CacheIndex(updateable = true)
	private String email;
	
	@NotNull @Size(min = 64, max = 64)
	@Column(nullable = false, updatable = true, length = 64)
	private String passwordHash;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "groupAlias", nullable = false, updatable = true)
	private Group group;
	
	@NotNull @Valid
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="country", column=@Column(name="country")),
		@AttributeOverride(name="city", column=@Column(name="city")),
		@AttributeOverride(name="street", column=@Column(name="street")),
		@AttributeOverride(name="postcode", column=@Column(name="postcode"))
	})
	private Address address;
	
	@NotNull @Valid
	@Embedded
	@AttributeOverrides({
		@AttributeOverride(name="title", column=@Column(name="title")),
		@AttributeOverride(name="given", column=@Column(name="forename")),
		@AttributeOverride(name="family", column=@Column(name="surname"))
	})
	private Name name;
	
	@NotNull 
	@ElementCollection
	@CollectionTable(
			schema = "radio",
			name = "PhoneAssociation",
			joinColumns = @JoinColumn(name = "personReference", nullable = false, updatable = true),
			uniqueConstraints = @UniqueConstraint(columnNames = {"personReference", "phone"})
	)
	@Column(name = "phone", nullable = false, updatable = true, length = 16)
	private Set<String> phones;
	
	// Relationen:
	@ManyToOne(optional = false) // means not null
	@JoinColumn(name = "avatarReference", nullable = false, updatable = true)
	private Document avatar;
	
	@NotNull
	@OneToMany(mappedBy = "negotiator", orphanRemoval = true, cascade = {CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH })
	private Set<Negotiation> negotiations;
	
	@NotNull
	@OneToMany(mappedBy = "owner", orphanRemoval = true, cascade = {CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH })
	private Set<Track> tracks;
	
	/*******************************************************************/
	//Constructor
	public Person() {	
		this.passwordHash = DEFAULT_PASSWORD_HASH;
		this.group = Group.USER;
		this.address = new Address();
		this.name = new Name();
		this.phones = new HashSet<>();
		this.tracks = Collections.emptySet();
		this.negotiations = Collections.emptySet();
	}
	
	/*******************************************************************/
	//Getter & Setter	
	@JsonbProperty @XmlAttribute
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String _email) {
		this.email = _email;
	}
	
	@JsonbProperty @XmlAttribute
	public String getPasswordHash( ) {
		return this.passwordHash;
	}
	public void setPasswordHash(String _passwordNotHashed) {
		this.passwordHash = HashCodes.sha2HashText(256, _passwordNotHashed);
	}
	
	@JsonbProperty @XmlAttribute
	public Group getGroup() {
		return group;
	}
	public void setGroup (Group _group) {
		this.group = _group;
	}
	
	@JsonbProperty @XmlElement
	public Name getName() {
		return name;
	}
	protected void setName(Name _name) {
		this.name = _name;
	}
	
	@JsonbProperty @XmlElement
	public Address getAddress() {
		return address;
	}
	protected void setAddress(Address _adresse) {
		this.address = _adresse;
	}
	
	public Set<String> getPhones() {
		return phones;
	}
	public void setPhones(Set<String> _phones) {
		this.phones = _phones;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getAvatarReference() {
		return this.avatar == null ? null : this.avatar.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Document getAvatar() {
		return avatar;
	}
	public void setAvatar(Document avatar) {
		this.avatar = avatar;
	}
	
	@JsonbProperty @XmlElement
	protected long[] getTrackReferences() {
		return this.tracks.stream().mapToLong(Track::getIdentity).sorted().toArray();
	}
	
	@JsonbTransient @XmlTransient	
	public Set<Track> getTracks() {
		return tracks;
	}
	protected void setTracks(Set<Track> tracks) {
		this.tracks = tracks;
	}
	
	@JsonbProperty @XmlElement
	public Set<Negotiation> getNegotiations() {
		return negotiations;
	}
	protected void setNegotiations(Set<Negotiation> negotiations) {
		this.negotiations = negotiations;
	}
}