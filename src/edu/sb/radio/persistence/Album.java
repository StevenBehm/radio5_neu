package edu.sb.radio.persistence;

import java.util.Collections;
import java.util.Set;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.JsonProtectedPropertyStrategy;


@Entity
@Table(schema = "radio", name = "Album")
@PrimaryKeyJoinColumn(name = "albumIdentity")
@JsonbVisibility(JsonProtectedPropertyStrategy.class)
@XmlType @XmlRootElement
@Copyright(year=2020, holders="Radio5 Group")

public class Album extends BaseEntity {
	/*******************************************************************/
	//Member
	@NotNull @Size(min = 0, max = 127)
	@Column(nullable = false, updatable = true, length = 127)
	private String title;
	
	@Column(nullable = false, updatable = true)
	private short releaseYear;
	
	@Column(nullable = false, updatable = true)
	private byte trackCount;

	@JoinColumn(nullable = true, updatable = false)
	private Document cover;
	
	//Relationen
	@NotNull
	@OneToMany(mappedBy = "album", orphanRemoval = true, cascade = {CascadeType.REMOVE, CascadeType.REFRESH, CascadeType.MERGE, CascadeType.DETACH })
	private Set<Track> tracks;
	
	/*******************************************************************/
	//Constructor
	public Album() {
		this.tracks =  Collections.emptySet();
	}
	
	/*******************************************************************/
	//Getter & Setter
	@JsonbProperty @XmlAttribute
	protected long getCoverReference() {
		return this.cover == null ? null : this.cover.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Document getCover() {
		return cover;
	}
	public void setCover(Document _cover) {
		cover = _cover;
	}
	
	@JsonbProperty @XmlAttribute
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String _title) {
		title = _title;
	}
	
	@JsonbProperty @XmlAttribute
	public short getReleaseYear() {
		return this.releaseYear;
	}
	public void setReleaseYear(short _releasY) {
		this.releaseYear = _releasY;
	}
	
	@JsonbProperty @XmlAttribute
	public byte getTrackCount() {
		return this.trackCount;
	}
	public void setTrackCount(byte _trackCount) {
		this.trackCount = _trackCount;
	}
	
	@JsonbProperty @XmlElement
	protected long[] getTrackReferences() {
		return this.tracks.stream().mapToLong(Track::getIdentity).sorted().toArray();
	}
	
	@JsonbTransient @XmlTransient
	public Set<Track> getTracks() {
		return tracks;
	}
	protected void setTracks(Set<Track> tracks) {
		this.tracks = tracks;
	} 
}
