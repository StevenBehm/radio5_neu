package edu.sb.radio.persistence;

import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;

@Embeddable
public class Name {
	/*******************************************************************/
	//Member
	@NotNull 
	@Size(min = 1, max = 31)
	@Column(nullable = true, updatable = true, length = 31)
	private String title;
	
	@NotNull 
	@Size(min = 1, max = 31)
	@Column(nullable = false, updatable = true, length = 31)
	private String given;
	
	@NotNull 
	@Size(min = 1, max = 31)
	@Column(nullable = false, updatable = true, length = 31)
	private String family;
	
	
	/*******************************************************************/
	//Getter & Setter
	@JsonbProperty @XmlAttribute
	public String getGiven() {
		return given;
	}
	public void setGiven(String given) {
		this.given = given;
	}

	@JsonbProperty @XmlAttribute
	public String getFamily() {
		return family;
	}
	public void setFamily(String family) {
		this.family = family;
	}
	
	@JsonbProperty @XmlAttribute
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}