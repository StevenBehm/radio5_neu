package edu.sb.radio.persistence;

import javax.json.bind.annotation.JsonbProperty;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;

@Embeddable
public class Address {
	/*******************************************************************/
	//Member
	@NotNull @Size(min = 0, max = 50) 
	@Column(nullable = false, updatable = true, length = 50)
	private String country;
	
	@NotNull @Size(min = 0, max = 63) 
	@Column(nullable = false, updatable = true, length = 63)
	private String city;
	
	@NotNull @Size(min = 0, max = 63)
	@Column(nullable = false, updatable = true, length = 63)
	private String street;
	
	@NotNull @Size(min = 0, max = 15)
	@Column(nullable = false, updatable = true, length = 15)
	private String postcode;
	
	/*******************************************************************/
	//Getter & Setter
	@JsonbProperty @XmlAttribute
	public String getCountry() {
		return this.country;	
	}
	public void setCountry(String _country) {
		this.country = _country;
	}
	
	@JsonbProperty @XmlAttribute
	public String getStreet() {
		return this.street;	
	}
	public void setStreet(String _street) {
		this.street = _street;
	}
	
	@JsonbProperty @XmlAttribute
	public String getCity() {
		return this.city;
	}
	public void setCity(String _city) {
		this.city = _city;
	}
	
	@JsonbProperty @XmlAttribute
	public String getPostcode() {
		return this.postcode;
	}
	public void setPostcode(String _postcode) {
		this.postcode = _postcode;
	}
}