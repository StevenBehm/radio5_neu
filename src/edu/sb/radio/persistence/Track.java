package edu.sb.radio.persistence;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.JsonProtectedPropertyStrategy;

@Entity
@Table(schema = "radio", name = "Track", indexes = { 
		@Index(columnList = "artist", unique = false),
		@Index(columnList = "genre", unique = false)
})
@PrimaryKeyJoinColumn(name = "trackIdentity")
@JsonbVisibility(JsonProtectedPropertyStrategy.class)
@XmlType @XmlRootElement
@Copyright(year=2020, holders="Radio5 Group")

public class Track extends BaseEntity {
	/*******************************************************************/
	@NotNull @Size(min = 1, max = 127)
	@Column(nullable = false, updatable = true, length = 127)
	private String name;
	
	@NotNull @Size(min = 1, max = 127)
	@Column(nullable = false, updatable = true, length = 127)
	private String artist;
	
	@NotNull @Size(min = 1, max = 31) 
	@Column(nullable = false, updatable = true, length = 31)
	private String genre;
	
	@PositiveOrZero
	@Column(nullable = false, updatable = false, insertable = true)
	private byte ordinal;
	
	//Member - Relationen
	@ManyToOne (optional = false)
	@JoinColumn(name = "recordingReference", nullable = false, updatable = true)
	private Document recording;
	
	@ManyToOne (optional = false)
	@JoinColumn(name = "albumReference", nullable = false, updatable = true)
	private Album album;
		
	@ManyToOne (optional = false)
	@JoinColumn(name = "ownerReference", nullable = false, updatable = true)
	private Person owner;
	

	/*******************************************************************/
	//Getter & Setter

	@JsonbProperty @XmlAttribute
	public String getName() {
		return this.name;
	}
	public void setName(String _name) {
		this.name = _name;
	}
	
	@JsonbProperty @XmlAttribute
	public String getGenre() {
		return this.genre;
	}
	public void setGenre(String _genre) {
		this.genre = _genre;
	}
	
	@JsonbProperty @XmlAttribute
	public String getArtist() {
		return this.artist;
	}
	public void setArtist(String _artist) {
		this.artist = _artist;
	}

	@JsonbProperty @XmlAttribute
	public byte getOrdinal() {
		return ordinal;
	}
	public void setOrdinal(byte _ordinal) {
		this.ordinal = _ordinal;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getRecordingReference() {
		return this.recording == null ? null : this.recording.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Document getRecording() {
		return recording;
	}
	public void setRecording(Document recording) {
		this.recording = recording;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getAlbumReference() {
		return this.album == null ? null : this.album.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Album getAlbum() {
		return album;
	}
	public void setAlbum(Album album) {
		this.album = album;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getOwnerReference() {
		return this.owner == null ? null : this.owner.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Person getOwner() {
		return owner;
	}
	public void setOwner(Person owner) {
		this.owner = owner;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getCoverReference() {
		return this.album == null ? null : this.album.getCoverReference();
	}
}
