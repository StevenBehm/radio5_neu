package edu.sb.radio.persistence;

import javax.json.bind.annotation.JsonbProperty;
import javax.json.bind.annotation.JsonbTransient;
import javax.json.bind.annotation.JsonbVisibility;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import edu.sb.radio.util.Copyright;
import edu.sb.radio.util.JsonProtectedPropertyStrategy;

@Entity
@Table(schema = "radio", name = "Negotiation", indexes = {
		@Index(columnList = "type", unique = false),
		@Index(columnList = "offer", unique = false),
		@Index(columnList = "answer", unique = false)
}) 
@PrimaryKeyJoinColumn(name = "negotiationIdentity")
@JsonbVisibility(JsonProtectedPropertyStrategy.class)
@XmlType @XmlRootElement
@Copyright(year=2020, holders="Radio5 Group")

public class Negotiation extends BaseEntity {
	/*******************************************************************/
	//Enum
	static public enum Type { WEB_RTC }
	
	/*******************************************************************/
	//Member
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, updatable = true)
	private Type type;

	@NotNull
	@Size(min = 1, max = 2046) 
	@Column(nullable = false, updatable = true, length = 2046)
	private String offer;
	
	@Size(min = 1, max = 2046)
	@Column(nullable = true, updatable = true, length = 2046)
	private String answer;
	
	@ManyToOne (optional = false)
	@JoinColumn(name = "negotiatorReference", nullable = false, updatable = true)
	private Person negotiator;
	
	/*******************************************************************/
	//Constructor
	public Negotiation() {
		this.type = Type.WEB_RTC;
	}
	
	/*******************************************************************/
	//Getter & Setter
	@JsonbProperty @XmlAttribute
	public Type getType() {
		return this.type;
	}
	public void setType (Type _type) {
		this.type = _type;
	}
	
	@JsonbProperty @XmlAttribute
	public String getOffer() {
		return this.offer;
	}
	public void setOffer(String _offer) {
		offer = _offer;
	}
	
	@JsonbProperty @XmlAttribute
	public String getAnswer() {
		return this.answer;
	}
	public void setAnswer(String _answer) {
		answer = _answer;
	}
	
	@JsonbProperty @XmlAttribute
	protected long getNegotiatorReference() {
		return this.negotiator == null ? null : this.negotiator.getIdentity();
	}
	
	@JsonbTransient @XmlTransient
	public Person getNegotiator() {
		return this.negotiator;
	}
	public void setNegotiator(Person _person) {
		negotiator = _person;
	}
	
	@JsonbProperty @XmlAttribute
	protected boolean isOffering() {
		return this.offer != null & this.answer == null;
	}
	
	@JsonbProperty @XmlAttribute
	protected boolean isAnswering() {
		return this.offer != null & this.answer != null;
	}
}
